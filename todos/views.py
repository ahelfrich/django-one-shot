from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todos/lists.html", context)


def todo_list_detail(request, id):
    tasks = TodoList.objects.get(id=id)
    context = {
        "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)

    context = {"form": form, "list": list}

    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = TodoItemForm(instance=item)

    lists = TodoList.objects.all()
    context = {"form": form, "item": item, "lists": lists}

    return render(request, "todos/items/update.html", context)
